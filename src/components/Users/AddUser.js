import React, { useState } from 'react';

import classes from './AddUser.module.css';

import Card from '../UI/Card';
import Button from '../UI/Button';
import ErrorModal from '../UI/ErrorModal';

const AddUser = (props) => {
	const [username, setUsername] = useState('');
	const [age, setAge] = useState('');

	const [error, setError] = useState(false);
	const [errorMessage, setErrorMessage] = useState('');

	const addUserHandler = (event) => {
		event.preventDefault();

		if (username.trim().length === 0 && age.trim().length === 0) {
			setErrorMessage("Please do not leave the forms empty.");
			return setError(true);
		}

		if (~~age < 1) {
			setErrorMessage("Age must be greater than 1");
			return setError(true);
		}

		props.fetchUsersHandler(username, age);

		setUsername('');
		setAge('');
	};

	const usernameChangeHandler = (e) => setUsername(e.target.value);
	const ageChangeHandler = (e) => setAge(e.target.value);

	const closeErrorModal = () => setError(false);

	return (
		<>
			{error && (
				<ErrorModal
					title="An error occured!"
					message={errorMessage}
					close={closeErrorModal}></ErrorModal>
			)}
			<Card className={classes.input}>
				<form onSubmit={addUserHandler}>
					<label htmlFor="username">Username</label>
					<input
						value={username}
						type="text"
						id="username"
						onChange={usernameChangeHandler}
					/>

					<label htmlFor="age">Age (Years)</label>
					<input
						value={age}
						type="number"
						id="age"
						onChange={ageChangeHandler}
					/>

					<Button type="submit">Add User</Button>
				</form>
			</Card>
		</>
	);
};

export default AddUser;
