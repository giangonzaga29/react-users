import React from 'react';

import classes from './UsersList.module.css';
import Card from '../UI/Card';

const UsersList = (props) => {
	return (
		<Card className={classes.users}>
			<ul>
				{props.users.map((user, i) => (
					<li key={i} onClick={() => props.deleteUserHandler(i)}>
						{user.username} ({user.age} years old)
					</li>
				))}
			</ul>
		</Card>
	);
};

export default UsersList;
