import React, { useState } from 'react';

import AddUser from './components/Users/AddUser';
import UsersList from './components/Users/UsersList';

function App() {
	const [users, setUsers] = useState([]);

	const fetchUsersHandler = (username, age) => {
		setUsers((prevUsers) => {
			return [
				...prevUsers,
				{
					username,
					age,
				},
			];
		});
	};

	const deleteUserHandler = (index) => {
		const usersCopy = [...users];
		usersCopy.splice(index, 1);
		setUsers(() => {
			return [...usersCopy];
		});
	};

	return (
		<div>
			<AddUser fetchUsersHandler={fetchUsersHandler} />
			<UsersList users={users} deleteUserHandler={deleteUserHandler} />
		</div>
	);
}

export default App;
